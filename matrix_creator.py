#==============================================================
#Author: Guillem Garcia Sala
#==============================================================
import numpy as np
import pandas as pd
import geopandas as gpd
import math


def Matrix(dimensio):
    if dimensio % 2 == 0:
        dimensio = dimensio + 1
    else:
        dimensio = dimensio

    matriu=np.zeros((dimensio,dimensio))

    CenterCell_=math.trunc(dimensio/2)
    matriu[CenterCell_,CenterCell_]=dimensio
    center=matriu[CenterCell_,CenterCell_]

    for i in range(dimensio):
        matriu[i]=i-CenterCell_

    eix_x=matriu.copy().T
    eix_y=matriu.copy()
    for j in range(dimensio):
        eix_y[j]=-(j-CenterCell_)



    z=np.reshape(eix_x,(dimensio*dimensio,1))
    w=np.reshape(eix_y,(dimensio*dimensio,1))
    union_zw=np.concatenate((z,w),axis=1)
    df_zw=pd.DataFrame(union_zw,columns=["lat","lon"])
    gdf_zw=gpd.GeoDataFrame(df_zw,geometry=gpd.points_from_xy(df_zw.lat,df_zw.lon))
    return gdf_zw



