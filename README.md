We have two options: Use the Function or use the Class(with the methods):

**Option1- Function**

Given a size (in odd integer format) as a parameter, it returns a matrix of points in GeodataFrame format (from the GeoPandas library) in the Cartesian plane where the origin point [0,0] is the center of the matrix.

*Example 01*
Given the Matrix () function
We enter the size value as a parameter and we use the matplotlib plot ()


```python
import matplotlib.pyplot as plt
from matrix_creator import Matrix

matriu=Matrix(33)
matriu.plot(color="green")
plt.show()
```

**Option 2- Class**

Using the class we have access to the methods:

* size (): Get the odd value of the array size
* center (): Get the central cell of the array
* matrixof0 () Create an array of the indicated size, empty (0)
* x_axis () Returns an array with the x-axis
* y_axis () Returns an array with the y-axis
* unio_xy () Returns an array with the x and y axis
* geodataframe () Returns the array database with x and y as the "geometry" element, georeferenced.
* distancies (cell_size) Returns the geodataframe with the distance of the origin point of each point.
* orientacions(cell_size)Returns the geodataframe with the orientation of the cartesian point from the origin point. Expressed in cardinal system(NSWE)
* bearing(cell_size)Returns the geodataframe with the bearing of each point to the origin. Expressed in degrees.

*Example 02* Using the class and the geodataframe () method and representing the array with plot ()

```python
from matrixclass import CreateMatriu
import matplotlib.pyplot as plt

matriu=CreateMatriu(5).geodataframe()
matriu.plot(color="green")
plt.show()
```
*Exemple 03* Using the class and the method distancies() and printing the six first lines as the output

```python
from matrixclass import CreateMatriu


matriu=CreateMatriu(5).distancies(100)
print(matriu.head())
```
output >>>

| lat | lon | geometry | dist_origin |
| ------ | ------ | ------ | ------ |
| -2.0| 2.0 | POINT (-2.00000 2.00000)|282.842712 |
| -1.0| 2.0 | POINT (-1.00000 2.00000)|223.606798 |
| 0.0 | 2.0 | POINT (0.00000 2.00000) |200.000000 |
| 1.0 | 2.0 | POINT (1.00000 2.00000) |223.606798 |
| 2.0 | 2.0 | POINT (2.00000 2.00000) |282.842712 |
| -2.0| 1.0 | POINT (-2.00000 1.00000)|223.606798 |



