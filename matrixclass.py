#==============================================================
#Author: Guillem Garcia Sala
#==============================================================
#TODO:Set the geographic coordinates for every point having the origin ones



import math
import pandas as pd
import geopandas as gpd
import numpy as np



class CreateMatriu:

    def __init__(self,size):
        self.size=size

    def size(self):
        if self.size % 2 == 0:
            self.size=self.size+1
        else:
            self.size=self.size
        return self.size

    def center(self):
        CenterCell=math.trunc(self.size/2)
        return CenterCell

    def matrixof0(self):
        matriu = np.zeros((self.size, self.size))
        matriu[self.center(),self.center() ] = self.size
        return matriu

    def eix_x(self):
        eix_x=self.matrixof0().copy()
        for i in range(self.size):
            eix_x[i]=i-self.center()
        eix_x=eix_x.T
        eix_x_reshape = np.reshape(eix_x, (self.size * self.size, 1))
        return eix_x_reshape

    def eix_y(self):
        eix_y=self.matrixof0().copy()
        for j in range(self.size):
            eix_y[j]=-(j-self.center())
        eix_y_reshape=np.reshape(eix_y,(self.size*self.size,1))
        return eix_y_reshape

    def unio_xy(self):
        unio_xy=np.concatenate((self.eix_x(),self.eix_y()),axis=1)
        return unio_xy

    def geodataframe(self):
        df_xy=pd.DataFrame(self.unio_xy(),columns=["lat","lon"])
        gdf_xy=gpd.GeoDataFrame(df_xy,geometry=gpd.points_from_xy(df_xy.lat,df_xy.lon))
        return gdf_xy

    def distancies(self,Cell_size):
        self.cell_size=Cell_size
        dist_x=abs(self.eix_x()*self.cell_size)**2
        dist_y= abs(self.eix_y() * self.cell_size) ** 2
        sqrt=np.sqrt(dist_x+dist_y)
        sqrtdf=pd.DataFrame(sqrt,columns=["dist"])
        frames=(self.geodataframe(),sqrtdf)
        dist_result=pd.concat(frames,axis=1)
        return dist_result

    def orientacio(self,Cell_size):

        orient_x = []
        for i in self.eix_x():
            if i < 0:
                orient_x.append("W", )
            elif i > 0:
                orient_x.append("E")
            else:
                orient_x.append("")

        orient_y = []
        for j in self.eix_y():
            if j < 0:
                orient_y.append("S", )
            elif j > 0:
                orient_y.append("N")
            else:
                orient_y.append("")

        orient_arr = np.array((orient_x, orient_y)).T

        df = pd.DataFrame(orient_arr, columns=["Direction_WE", "Direction_NS"])
        frames=(self.distancies(Cell_size),df)
        orient_result = pd.concat(frames, axis=1)
        return orient_result

    def bearing(self,Cell_size):
        from calculate_bearing import calculate_bearing
        matriu_xy = self.unio_xy()
        df = pd.DataFrame(matriu_xy, columns=["x", "y"])
        bearing = []
        for a, b in df.itertuples(index=False):
            bearing.append(calculate_bearing((0, 0), (b, a)))

        df_bearing = pd.DataFrame(bearing, columns=["bearing"])
        frames = (self.orientacio(Cell_size),df_bearing)
        bearing_result=pd.concat(frames,axis=1)
        return bearing_result














